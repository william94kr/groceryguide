- Name, EID, and GitLab ID, of all members

|Name              |EID    |Gitlab ID        |
|---               |---    |---              |
|Justin Foster     |jdf3434|justindeanfoster |
|Mathew Tucciarone|mat4987|mat4987          |
|Aditya Gupta      |ag68834|aditya2000       |
|Kevin Li          |kal3558|li.kevin         |
|William Kim       |sk45248|william94kr      |

- Git SHA: 4c398541d39967c7c29531b28335610348462007

- Project Leader: Justin Foster

- Link to GitLab pipelines: https://gitlab.com/justindeanfoster/groceryguide/-/pipelines

- Link to website: https://www.groceryguide.tech

- Estimated completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |15   |
|Mattew Tucciarone |15   |
|Aditya Gupta       |20   |
|Kevin Li           |20   |
|William Kim        |20   |

- Actual completion time for each member (hours: int)

|Name               |Hours|
|---                |---  |
|Justin Foster      |  25  |
|Mathew Tucciarone  |  15  |
|Aditya Gupta       |  27  |
|Kevin Li           |  32  |
|William Kim        |  27  |

Comments:

Our view all page for the Stores model is at https://www.groceryguide.tech/stores/walmart and not https://www.groceryguide.tech/stores. The `/stores` page was created to set up for the next phase of directly filtering based on store chains.  

**Pages**:  
Home: https://www.groceryguide.tech/home  

About Us: https://www.groceryguide.tech/about  


**Model #1: Stores**  
View all: https://www.groceryguide.tech/stores/walmart  
Instance #1: https://www.groceryguide.tech/stores/walmart/1  
Instance #2: https://www.groceryguide.tech/stores/walmart/2  
Instance #3: https://www.groceryguide.tech/stores/walmart/3  

**Model #2: Cities**  
View all: https://www.groceryguide.tech/locations  
Instance #1: https://www.groceryguide.tech/locations/Austin  
Instance #2: https://www.groceryguide.tech/locations/Dallas  
Instance #3: https://www.groceryguide.tech/locations/SanAntonio  

**Model #3: Items**  
View all: https://www.groceryguide.tech/items  
Instance #1: https://www.groceryguide.tech/items/1  
Instance #2: https://www.groceryguide.tech/items/2  
Instance #3: https://www.groceryguide.tech/items/3  
