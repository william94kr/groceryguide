import React from 'react';
import './store-lists//storeList.css'
import { PageHeader, Table, Tag, Descriptions } from 'antd'
import { Link } from 'react-router-dom'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: text => <Link to='/void'>{text}</Link>,
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        render: price => {
            
            let color = 'green'
            if (price === '$') {
                color = 'red'
            } else if (price === '$$') {
                color = 'gold'
            } else if (price == null) {
                color = 'purple'
            }

            return (
                <Tag color={ color } key = { price }>
                    { price == null ? '?' : '$'}
                </Tag>
            )
            
        }
    },
    {
        title: 'Open',
        dataIndex: 'open',
        key: 'open'
    },
    {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating'
    },
];

const data = [
    {
        key: '1',
        name: 'Target',
        city: 'Austin, 78705',
        price: '$$',
        open: 'yes',
        rating: '3.5'
    },
    {
        key: '2',
        name: 'Target',
        city: 'Austin, 78723',
        price: '$$',
        open: 'yes',
        rating: '3.0'
    },
    {
        key: '3',
        name: 'Target',
        city: 'Austin, 78702',
        price: null,
        open: 'yes',
        rating: '4.5'
    },
];

class StoreList extends React.Component {
    render() {
        return <>
           <div className='store-list-page-header'>
                <PageHeader
                ghost={false}
                onBack={() => window.history.back()}
                title='Target'
                subTitle='Showing all locations for Target.'
                >
                <Descriptions size='small' column={3}>
                    <Descriptions.Item label='Number of Instances'>0</Descriptions.Item>
                </Descriptions>
                <Descriptions size='small' column={3}>
                    <Descriptions.Item label='Number of Pages'>1</Descriptions.Item>
                </Descriptions>
                </PageHeader>
            </div>
            <div className='store-list-page-header'>    
                <Table columns={ columns } dataSource={ data }/>
            </div>
        </>
    }
}
export default StoreList;
