import React from 'react';
import { Image,Button,Tag,Table ,Divider} from 'antd';
import { Col, Row, Typography, } from 'antd';
import LifeScoreData from '../static/LifeScore.json';
import { Link } from 'react-router-dom'
import DallasData from './static/DallasItems.json'
import ItemCard from '../../components/ItemCard'
var items = DallasData.items
const { Title, Paragraph } = Typography;

const properties = [
    {
        "id": 118355,
        "wikiDataId": "Q16557",
        "type": "CITY",
        "city": "Dallas",
        "name": "Dallas",
        "country": "United States of America",
        "countryCode": "US",
        "region": "Texas",
        "regionCode": "TX",
        "elevationMeters": 131,
        "latitude": 32.779166666,
        "longitude": -96.808888888,
        "population": 1197816,
        "timezone": "America__Chicago",
        "deleted": false,
    }
]

const StoreCol = [
    {
        title: 'Store',
        dataIndex: 'num',
        key: 'num',
        render: num => {
            let url = `/stores/walmart/${num}`
            return <Link to={url}>
                <Button type='primary'> Go to store </Button>
            </Link>
        }
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
    },
    {
        title: 'Open',
        dataIndex: 'open',
        key: 'open'
    },
    {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating'
    },
    {
        title: 'Amenities',
        dataIndex: 'amenitiesOffered',
        key: 'amenitiesOffered',
        render: amenities => {
            return amenities.map(amenity => {
                let color = 'magenta'
                if (amenity === 'Curbside Pickup') {
                    color = 'cyan'
                } else if (amenity === 'Delivery') {
                    color = 'red'
                }
                return <Tag color={color} key={amenity}>
                    {amenity}
                </Tag>

            })
        }
    },

];

const Storedata=[
    {
        key: '2',
        num: '2',
        name: 'Walmart Supercenter',
        city: 'Dallas, 75231',
        price: '$',
        open: 'yes',
        rating: '2.0',
        amenitiesOffered: ['Masks Required', 'Delivery'],
       
    },
]
const columns = [
    {
        title: 'Population',
        dataIndex: 'population',
        key: 'population',
    },
    {
        title: 'Elevation',
        dataIndex: 'elevationMeters',
        key: 'elevationMeters',
    },
    {
        title: 'Time zone',
        dataIndex: 'timezone',
        key: 'timezone',
    },
    {
        title: 'Region',
        dataIndex: 'regionCode',
        key: 'regionCode',
    },

];

const columnsScore = [
    {
        title: 'Feature',
        dataIndex: 'name',
        key: 'feature',
    },
    {
        title: 'Score out of 10',
        dataIndex: 'score_out_of_10',
        key: 'score'
    },
];

function ImageDemo() {
    return (
        <div style={{ textAlign: 'center' }}>
        <Image
            width={800}
            src="https://media.tacdn.com/media/attractions-splice-spp-674x446/06/6a/e5/73.jpg"
        />
        </div>
    );
}
function DallasPic() {
    return (
        <div style={{ textAlign: 'center' }}>
        <Image
            width={400}
            src="https://i.pinimg.com/originals/4f/07/d5/4f07d5cf9b919967540c97f1af855a9c.gif"
        />
            </div>
            

    );
}


const Dallas = () => {
    const itemCards = items.map(prop => {
        prop.isStore = false
        return <ItemCard {...prop} />
    })


    return <div className='page-header-wrapper'>
        <Typography>
        <Divider>
                <Title style={{ textAlign: 'center' }}>Dallas, Texas</Title>
            </Divider>
            <ImageDemo />
            <br/>
            <br/>
            <div>
                <Table dataSource={properties} columns={columns} />
            </div>

            <Paragraph>
                <Divider>
                    <h3>Life Score of Dallas</h3>
                </Divider>
                <br/>
             <div>
                {/* API from Teleport */}
                {/* https://api.teleport.org/api/cities/?search=Dallas&embed=city%3Asearch-results%2Fcity%3Aitem%2Fcity%3Aurban_area%2Fua%3Ascores */}
                <Table dataSource= {LifeScoreData.Dallas} columns= {columnsScore} pagination= {{ pageSize: 50 }} scroll= {{ y: 400 }}/>
             </div>
            </Paragraph>

            <Paragraph>
            <Divider>
                <h3> Recommended Stores </h3>
            </Divider>
                
          </Paragraph>
          <Table columns={StoreCol} dataSource={Storedata} />
          <Paragraph>

                <Divider> <h3> Items Offered </h3> </Divider>
                <div className='page-header-wrapper'>
                    <Row gutter={20}>
                        <Col span={8}>
                            {itemCards[0]}
                        </Col>
                        <Col span={8}>
                            {itemCards[1]}
                        </Col>
                        <Col span={8}>
                            {itemCards[2]}
                        </Col>
                    </Row>
                </div>
            </Paragraph>
           
            <Paragraph style={{ textAlign: 'center' }}>Nearby Stores in San Antonio</Paragraph>
            <DallasPic />
        </Typography>
    </div>
}

export default Dallas;