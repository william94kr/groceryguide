import AustinData from './static/AustinItems.json'
import ItemCard from '../../components/ItemCard'
import React from 'react';
import { Col, Divider, Row, Typography } from 'antd';
import { Image, Button, Tag } from 'antd';
import { Table } from 'antd';
import LifeScoreData from '../static/LifeScore.json';
import { Link } from 'react-router-dom'
const { Title, Paragraph } = Typography;
var items = AustinData.items

//API used to scrub info
//https://rapidapi.com/wirefreethought/api/geodb-cities/

const StoreCol = [
    {
        title: 'Store',
        dataIndex: 'num',
        key: 'num',
        render: num => {
            let url = `/stores/walmart/${num}`
            return <Link to={url}>
                <Button type='primary'> Go to store </Button>
            </Link>
        }
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        // render: _ => {
        //     return `$${this.props.price}`
        // }
    },
    {
        title: 'Open',
        dataIndex: 'open',
        key: 'open'
    },
    {
        title: 'Rating',
        dataIndex: 'rating',
        key: 'rating'
    },
    {
        title: 'Amenities',
        dataIndex: 'amenitiesOffered',
        key: 'amenitiesOffered',
        render: amenities => {
            return amenities.map(amenity => {
                let color = 'magenta'
                if (amenity === 'Curbside Pickup') {
                    color = 'cyan'
                } else if (amenity === 'Delivery') {
                    color = 'red'
                }
                return <Tag color={color} key={amenity}>
                    {amenity}
                </Tag>

            })
        }
    },

];


const Storedata = [
    {
        key: '1',
        num: '1',
        name: 'Walmart Supercenter',
        city: 'Austin, 78704',
        price: '$',
        open: 'yes',
        rating: '2.0',
        amenitiesOffered: ['Masks Required', 'Delivery', 'Curbside Pickup'],

    },

];

const properties =
    [
        {
            "id": 118913,
            "wikiDataId": "Q16559",
            "type": "CITY",
            "city": "Austin",
            "name": "Austin",
            "country": "United States of America",
            "countryCode": "US",
            "region": "Texas",
            "regionCode": "TX",
            "elevationMeters": 149,
            "latitude": 30.3,
            "longitude": -97.733333333,
            "population": 912791,
            "timezone": "America__Chicago",

            "deleted": false,
        }
    ]

const columns = [
    {
        title: 'Population',
        dataIndex: 'population',
        key: 'population',
    },
    {
        title: 'Elevation',
        dataIndex: 'elevationMeters',
        key: 'elevationMeters',
    },
    {
        title: 'Time Zone',
        dataIndex: 'timezone',
        key: 'timezone',
    },
    {
        title: 'Region',
        dataIndex: 'regionCode',
        key: 'regionCode',
    },

];

const columnsScore = [
    {
        title: 'Feature',
        dataIndex: 'name',
        key: 'feature',
    },
    {
        title: 'Score out of 10',
        dataIndex: 'score_out_of_10',
        key: 'score'
    },
];


function ImageDemo() {
    return (
        <div style={{ textAlign: 'center' }}>

            <Image
                width={800}
                src="https://cdn.theculturetrip.com/wp-content/uploads/2019/01/fa922p.jpg"
            />
        </div>

    );
}

function AustinPic() {
    return (
        <div style={{ textAlign: 'center' }}>
            <Image
                width={400}
                src="https://www.google.com/maps/d/thumbnail?mid=1k-V-3ZwOKbk49SnJ3YY6W4iLo7I"
            />
        </div>
    );
}


const Austin = () => {


    const itemCards = items.map(prop => {
        prop.isStore = false
        return <ItemCard {...prop} />
    })



    return <div className='page-header-wrapper'>
        <Typography>
            <Divider>
                <Title style={{ textAlign: 'center' }}>Austin, Texas</Title>
            </Divider>
            <ImageDemo />
            <br />
            <br />
            <div>
                <Table dataSource={properties} columns={columns} />
            </div>

            <Paragraph>
                <Divider>
                    <h3>Life Score of Austin</h3>
                </Divider>
                <br />
                <div>
                    {/* API from Teleport */}
                    {/* https://api.teleport.org/api/cities/?search=Austin&embed=city%3Asearch-results%2Fcity%3Aitem%2Fcity%3Aurban_area%2Fua%3Ascores */}
                    <Table dataSource={LifeScoreData.Austin} columns={columnsScore} pagination={{ pageSize: 50 }} scroll={{ y: 400 }} />
                </div>
            </Paragraph>
            <Paragraph>
                <Divider>
                    <h3> Recommended Stores </h3>
                </Divider>


                <Table columns={StoreCol} dataSource={Storedata} />
            </Paragraph>
            <Paragraph>

                <Divider> <h3> Items Offered </h3> </Divider>
                <div className='page-header-wrapper'>
                    <Row gutter={20}>
                        <Col span={8}>
                            {itemCards[0]}
                        </Col>
                        <Col span={8}>
                            {itemCards[1]}
                        </Col>
                        <Col span={8}>
                            {itemCards[2]}
                        </Col>
                    </Row>
                </div>
            </Paragraph>

            <Paragraph style={{ textAlign: 'center' }}>Nearby Stores in Austin</Paragraph>

            <AustinPic />

        </Typography>
    </div>
}

export default Austin;