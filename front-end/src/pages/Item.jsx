import React from 'react';
import 'antd/dist/antd.css';
import './styles/item.css'
import ItemCard from '../components/ItemCard';
import { PageHeader, Row, Col, Descriptions, Input, Popover } from 'antd';
import item1 from './item-instances/static/item1.json'
import item2 from './item-instances/static/item2.json'
import item3 from './item-instances/static/item3.json'

const { Search } = Input

// Do nothing on search for now.
const onSearch = value => console.log(value);

// Values for 3 Items.
const properties = [ item1, item2, item3 ]

const ItemCards = properties.map( prop => <ItemCard {...prop}/>)

class Item extends React.Component {
    handleVisibleChange = visible => {
        this.setState({ visible });
    };

    render() {
        return (
            <>
                <div className='page-header-wrapper'>
                    <PageHeader
                    ghost={false}
                    title='Available Items'
                    subTitle='Browse/search for available Items'
                    >
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Last Updated'>10/2/2021</Descriptions.Item>
                        </Descriptions>
                        <Popover
                            title="Search is still a work in progress!"
                            trigger="click"
                            onVisibleChange={this.handleVisibleChange}
                        >
                            <Search
                            placeholder='Apple'
                            allowClear
                            enterButton='Search'
                            size='large'
                            onSearch={onSearch}
                            />
                        </Popover>
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Instances'>3</Descriptions.Item>
                        </Descriptions>
                        <Descriptions size='small' column={3}>
                            <Descriptions.Item label='Number of Pages'>1</Descriptions.Item>
                        </Descriptions>
                    </PageHeader>
                </div>
    
    
                <div className="page-header-wrapper">
                    <Row gutter={20}>
                        <Col span={8}>
                            { ItemCards[0] }
                        </Col>
                        <Col span={8}>
                            { ItemCards[1] }
                        </Col>
                        <Col span={8}>
                            { ItemCards[2] }
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

export default Item