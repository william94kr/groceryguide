import React from 'react';
import { Card, Col, Row, Spin } from 'antd';
import TeamMemberCard from '../components/TeamMemberCard';
import ToolCard from '../components/ToolCard'
import { Typography, Divider } from 'antd';
import './styles/about.css'

import kevinImage from './static/kevinli.png'
import justinImage from './static/justinfoster.png'
import adityaImage from './static/adityagupta.png'
import mathewImage from './static/mathewtucci.png'
import sungwookImage from './static/sungwookkim.png'

const { Meta } = Card
const { Title } = Typography

const gitLabInfo = async () => {
  const teamInfo = [
    {
      name: 'Kevin Li',
      alias: null,
      username: 'li.kevin',
      email: 'likevinsen@gmail.com',
      role: 'Full-Stack',
      image: kevinImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: `
        Kevin Li is a Junior at the University of Texas at Austin studying Computer Science. In the past,
        he's worked at ClassPass as a Software Engineering Intern on the Machine Learning + Data team. In
        his free time, he enjoys playing ultimate frisbee, running, and walking his dog.
      `
    },
    {
      name: 'Aditya Gupta',
      alias: 'AdityaG2000',
      username: 'aditya',
      email: 'Aditya2000@utexas.edu',
      role: 'Full-Stack',
      image: adityaImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: 'Aditya Gupta is a Senior at the University of Texas studying Computer Science. He worked in the past at Amazon and Intel as a Software Engineering Intern. In his free time, he enjoys playing video games, running and learning new algorithms',
    },
    {
      name: 'Justin Foster',
      alias: null,
      username: 'justinfoster',
      email: 'jrfoster2416@gmail.com',
      role: 'Full-Stack',
      image: justinImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: `
            Justin Foster is a Junior at the University of Texas - Austin studying Computer Science. 
            He loves to build and create, whether that is legos or complex software applications. 
            Passionate about all things space and wanting to work in Space Exploration and research, 
            Justin is currently involved with Texas Spacecraft Laboratory developing machine learning tools
             to research pose estimation for oribiting satellites.`
    },
    {
      name: 'Mathew Tucciarone',
      username: 'mat4987',
      email: 'mat4987@utexas.edu',
      role: 'Full-Stack',
      image: mathewImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: 'Mathew Tucciarone is a Senior Computer Science Major. He worked as a Jr. Software Developer for Resermine Inc doing Web development. In his free time he enjoys watching sports such as the Texas Longhorns beating Oklahoma. He is on the University of Texas Ice Hockey Team.'
    },
    {
      name: 'William (Sungwook) Kim',
      alias: 'Sungwook',
      username: 'william94kr',
      email: 'brian94kr@gmail.com',
      role: 'Full-Stack',
      image: sungwookImage,
      commits: 0,
      issues: 0,
      tests: 0,
      bio: ` Sungwook Kim is a Junior at the University of Texas at Austin studying Computer Science. 
             He is interested in the automation system and IoT(Internet of Things), and his goal is to work 
             on them such as contributing to "Amazon Go" which is an automated checkout Amazon stores.  
             In his free time, he enjoys playing golf, bowling, and singing in Karaoke.
            `
    }
  ]

  const urls = [
    'https://gitlab.com/api/v4/projects/29881146/issues?per_page=1000',
    'https://gitlab.com/api/v4/projects/29881146/repository/contributors'
  ]
  const rawData = await Promise.all(
    urls.map(
      async (url) => (await fetch(url)).json()
    )
  )



  const issues = rawData[0]
  const contributors = rawData[1]

  for (let issue of issues) {
    const { name, username } = issue.author

    for (let member of teamInfo) {
      if (name === member.name || name === member.alias || username === member.username) {
        member.issues += 1
        break
      }
    }
  }

  let totalCommits = 0
  for (let contributor of contributors) {
    console.log(contributor)
    for (let member of teamInfo) {
      const { name, email, commits } = contributor
      if (name === member.name || name === member.alias || email === member.email) {
        member.commits += commits
        totalCommits += commits
      }
    }
  }

  const totalIssues = issues.length
  return {
    team: teamInfo,
    commits: totalCommits,
    issues: totalIssues,
    tests: 0
  }
}

const tools = [
  {
    name: 'Docker',
    image: 'https://miro.medium.com/max/336/1*glD7bNJG3SlO0_xNmSGPcQ.png',
    description: 'Used to containerize applications for deployment.',
    link: 'https://www.docker.com/'
  },
  {
    name: 'AWS',
    image: 'https://yt3.ggpht.com/ytc/AKedOLQP0vNXjkoKrCAYvWyOm9vEhDuBNytjbpEYi1ugD7w=s900-c-k-c0x00ffffff-no-rj',
    description: 'Used to host/deploy website over AWS Amplify',
    link: 'https://aws.amazon.com/'
  },
  {
    name: 'React',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png',
    description: 'Used to create frontend components and pages',
    link: 'https://reactjs.org/'
  },
  {
    name: 'Postman',
    image: 'https://mms.businesswire.com/media/20210818005151/en/761650/22/postman-logo-vert-2018.jpg',
    description: 'Used to request information from external APIs. Also used for unit testing endpoints',
    link: 'https://www.postman.com/'
  },
  {
    name: 'GitLab',
    image: 'https://images.g2crowd.com/uploads/product/image/social_landscape/social_landscape_15680ee909406e13c21c8f179f83d99e/gitlab.png',
    description: 'Used for hosting the code repository, issue tracking, and continuous integration (via GitLab pipeline).',
    link: 'https://about.gitlab.com/'
  },
  {
    name: 'Yelp Fusion API',
    image: 'https://www.adweek.com/wp-content/uploads/2021/08/YelpLogoAugust2021.jpg',
    description: 'Used to get information regarding store instance hours, amenities offered, reviews, and address.',
    link: 'https://www.yelp.com/fusion'
  },
  {
    name: 'Google Maps API',
    image: 'https://i.pcmag.com/imagery/reviews/05BN3njlO8GzG0L7AvjC5Od-5.1611688960.fit_lim.size_1050x591.jpg',
    description: 'Used to show location of the physical store address.',
    link: 'https://developers.google.com/maps'
  },
  {
    name: 'Open Food Facts',
    image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Open_Food_Facts_logo.svg/1200px-Open_Food_Facts_logo.svg.png',
    description: 'Used to obtain nutritional information regarding grocery store items.',
    link: 'https://world.openfoodfacts.org/data'
  },
  {
    name: 'Walmart',
    image: 'https://s3.amazonaws.com/www-inside-design/uploads/2018/04/walmart-square.jpg',
    description: 'Used to check for availability of items in specific physical stores.',
    link: 'https://www.walmart.com/'
  },
  {
    name: 'Teleport API',
    image: 'https://teleport.org/assets/firefly/logo.png',
    description: 'Used to obtain information/statistics about cities.',
    link: 'https://developers.teleport.org/api/'
  }
]

const toolCards = tools.map((tool) => {
  return <ToolCard {...tool}></ToolCard>
})
class About extends React.Component {
  constructor() {
    super()
    this.state = { data: null }
  }
  async componentDidMount() {
    const resp = await gitLabInfo()
    this.setState({ data: resp.team, commits: resp.commits, issues: resp.issues, tests: resp.tests })
  }
  render() {
    return <>
      <Divider>
        <Typography>
          <Title level={2}>What is <b className='bolded'>Grocery</b>Guide? </Title>
        </Typography>
      </Divider>
      <div style={{ padding: '15px', paddingTop: '0px' }}>
        <Row gutter={16}>
          <Col span={12}>
            <Card
              bordered={false}
              className='store-info-card'
            >
              <Meta
                title={
                  <Typography>
                    <Title level={4}> Our Mission Statement </Title>
                  </Typography>
                }
                description={
                  <div>
                    <b className='bolded'>Grocery</b>Guide is an all-in-one grocery store aggregator. Our website
                                makes it easy to search for local stores, compare prices, and plan for your next shopping trip.
                                <br></br><br></br>
                                In response to the prevalence of <a href='https://foodispower.org/access-health/food-deserts/'>food
                                deserts</a>, we take extra care in promoting a variety of healthy/nutritious grocery options. For each
                                item listed in the store, nutrition facts are provided in addition to appropriate labeling (USDA
                                Organic, Vegeterian, etc...) and a <a href='https://world.openfoodfacts.org/nutriscore'>Nutri-score</a>.
                        </div>
                }
              ></Meta>

            </Card>

          </Col>
          <Col span={12}>
            <Card
              bordered={false}
              className='store-info-card'
            >
              <Meta
                title={
                  <Typography>
                    <Title level={4}> Data Insights </Title>
                  </Typography>
                }
                description={
                  <div>
                    We scrape from a variety of APIs and websites (including Yelp, Google Places,
                    Open Food Facts) to aggregate grocery store locations and item prices. We are
                    able to glean important insights about:
                        <br></br>
                    <ol>
                        <li> Where consumers can find the lowest grocery prices. </li>
                        <li> Distribution of stores that offer organic, fair-trade options within cities. </li>
                        <li> Which stores/chains have grocery store items with the highest nutritional value. </li>
                    </ol>
                    <div>Interesting Observations:</div>
                    <ol>
                        <li> 
                            Prices change over geographical region and cities, even if they come from the same chain.
                            We also noticed that the items in inventory differed greatly when stores were physically more
                            distant.
                        </li>
                        <li>
                            Stores from the same chain will usually only have one store per major region/zipcode. It 
                            is very rare for there to be multiple stores from the same chain physically close together.
                        </li>
                    </ol>
                  </div>
                }
              ></Meta>
            </Card>
          </Col>
        </Row>
        <Divider>
          <Typography>
            <Title level={2}> The Team </Title>
          </Typography>
        </Divider>
        {
          this.state.data ?
            <>
              <Row gutter={16} justify='center'>
                <Col span={6}>
                  <TeamMemberCard {...this.state.data[0]} />
                </Col>
                <Col span={6}>
                  <TeamMemberCard {...this.state.data[1]} />
                </Col>
                <Col span={6}>
                  <TeamMemberCard {...this.state.data[2]} />
                </Col>
              </Row>
              <br />
              <Row gutter={16} justify='center'>
                <Col span={6}>
                  <TeamMemberCard {...this.state.data[3]} />
                </Col>
                <Col span={6}>
                  <TeamMemberCard {...this.state.data[4]} />
                </Col>
              </Row>
            </> : <div style={{ textAlign: 'center' }}> <Spin size='large' /> </div>
        }
      </div>
      <Divider>
        {
          this.state.data ?
            <Card
              bordered={false}
              className='store-info-card'
            >
              <Meta
                title={
                  <Typography>
                    <Title level={4}> Additional Information </Title>
                  </Typography>
                }
                description={
                  <div>
                    <b> Total Commits: </b> {this.state.commits} <br />
                    <b> Total Issues: </b> {this.state.issues} <br />
                    <b> Total Tests: </b> {this.state.tests} <br />

                    <a href='https://gitlab.com/justindeanfoster/groceryguide'>GitLab Repo</a>,
                        <a href='https://documenter.getpostman.com/view/12795557/UUy67QoJ#f5abbfa1-2f39-4fe5-be3d-b3939ea82ee0'> Postman</a>.
                    </div>
                }
              ></Meta>
            </Card> : <div style={{ textAlign: 'center' }}> <Spin size='large' /> </div>
        }
        <br/>
        <Typography>
          <Title level={2}> APIs/Tools Used </Title>
        </Typography>
      </Divider>

      <Row gutter={16}>
        <Col span={6}>
          {toolCards[0]}
        </Col>
        <Col span={6}>
          {toolCards[1]}
        </Col>
        <Col span={6}>
          {toolCards[2]}
        </Col>
        <Col span={6}>
          {toolCards[3]}
        </Col>
      </Row>
      <br />
      <Row gutter={16}>
        <Col span={6}>
          {toolCards[4]}
        </Col>
        <Col span={6}>
          {toolCards[5]}
        </Col>
        <Col span={6}>
          {toolCards[6]}
        </Col>
        <Col span={6}>
          {toolCards[7]}
        </Col>
      </Row>
      <br />
      <Row gutter={16} justify='center'>
        <Col span={6}>
          {toolCards[8]}
        </Col>
        <Col span={6}>
          {toolCards[9]}
        </Col>
      </Row>
    </>
  }
}

export default About;