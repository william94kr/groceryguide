import React from 'react';
import 'antd/dist/antd.css';
import './storeInstance.css'
import { Card, Row, Col, PageHeader, Divider } from 'antd'
import { EnvironmentOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom'
import MapContainer from '../../components/MapContainer';
import { Table, Tag } from 'antd';
import StarRatings from 'react-star-ratings';
import ItemCard from '../../components/ItemCard'

const { Meta } = Card


const colorMappingAmenities = {
    'Masks Required': 'magenta',
    'Delivery': 'red',
    'Curbside Pickup': 'cyan'
}

const colorMappingPrice = {
    'Low': 'green',
    'Medium': 'yellow',
    'High': 'red',
    'N/A': 'purple'
}

class StoreInstance extends React.Component {

    getCityLink() {
        return `/locations/${this.props.city}`
    }

    render() {

        const columns = [
            {
                title: 'Day',
                dataIndex: 'day',
                key: 'day',
            },
            {
                title: 'Hours',
                dataIndex: 'hours',
                key: 'hours',
            }
        ]
        const itemProperties = this.props.items

        const itemCards = itemProperties.map(prop => {
            prop.isStore = true
            return <ItemCard {...prop} />
        })


        const hoursData = this.props.hours.map((hourObj, idx) => {
            return {
                key: idx + 1,
                day: hourObj.day,
                hours: `${hourObj.start} - ${hourObj.end}`
            }
        })
        const real_city_name = this.props.city === 'SanAntonio' ? 'San Antonio' : this.props.city

        return <>
            <div className='store-list-page-header'>
                <PageHeader
                    ghost={false}
                    onBack={() => window.history.back()}
                    title={this.props.storeName}
                    subTitle='Overview'
                />
            </div>
            <div style={{ padding: '30px' }}>
                <Row gutter={16}>
                    <Col span={8}>
                        <Card
                            bordered={false}
                            cover={
                                <img
                                    src={this.props.imageLink}
                                    className='cropped-image'
                                    alt={this.props.storeName}
                                />
                            }
                            className='store-info-card'
                        >
                            <Meta
                                title={<div><EnvironmentOutlined className='instance-icon' /> Walmart Supercenter </div>}
                                description={
                                    <div>
                                        <b> Address: </b> <p> {this.props.address} </p>
                                        <b> Phone: </b> <p> {this.props.phone} </p>
                                        <b> City: </b> <p> {<Link to={this.getCityLink()}> {real_city_name} </Link>}
                                        </p>
                                        <b> ZIP code: </b> <p> {this.props.zipCode} </p>
                                        <b> State: </b> <p> {this.props.state} </p>
                                        <StarRatings
                                            starDimension="20px"
                                            starSpacing="10px"
                                            rating={this.props.rating}
                                            numberOfStars={5}
                                            starRatedColor={'#90EE90'}
                                        />
                                    </div>
                                }
                            ></Meta>
                        </Card>
                    </Col>
                    <Col span={10}>
                        <Card
                            title='General Information'
                            bordered={false}
                            className='store-info-card'
                        >
                            Amenities Offered: <br />
                            <div>
                                {
                                    this.props.amenitiesOffered.map((amenity) => {
                                        let color = 'gray'
                                        if (amenity in colorMappingAmenities) {
                                            color = colorMappingAmenities[amenity]
                                        }
                                        return <Tag color={color}> {amenity} </Tag>
                                    })
                                }
                            </div>
                            <br />
                    Cost: <br />
                            <div>
                                <Tag color={colorMappingPrice[this.props.price]}>
                                    {this.props.price}
                                </Tag>
                            </div>
                            <br />
                            <Table columns={columns} dataSource={hoursData} pagination={false} />

                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card
                            title="Maps"
                            bordered={false}
                            className='store-info-card'
                        >
                            <MapContainer {...this.props.coordinates} />
                        </Card>
                    </Col>
                </Row>

                <Divider>Items Offered</Divider>
                <div className='page-header-wrapper'>
                    <Row gutter={20}>
                        <Col span={8}>
                            {itemCards[0]}
                        </Col>
                        <Col span={8}>
                            {itemCards[1]}
                        </Col>
                        <Col span={8}>
                            {itemCards[2]}
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    }
}
export default StoreInstance
