import React from 'react';
import PageMenu from './components/Navbar'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

// View all pages.
import Home from './pages/Home';
import Store from './pages/Store'
import Location from './pages/Location'
import About from './pages/About'
import Item from './pages/Item'
import StoreList from './pages/store-lists/StoreList'

// Import instance pages
import Target from './pages/Target'
import Austin from './pages/location-instances/Austin';
import Dallas from './pages/location-instances/Dallas';
import SanAntonio from './pages/location-instances/SanAntonio';
import Heb from './pages/Heb'
import StoreInstance from './pages/store-instances/StoreInstance'
import ItemInstance from './pages/item-instances/ItemInstance'
import walmart1 from './pages/store-instances/static/walmart1.json'
import walmart2 from './pages/store-instances/static/walmart2.json'
import walmart3 from './pages/store-instances/static/walmart3.json'
import item1 from './pages/item-instances/static/item1.json'
import item2 from './pages/item-instances/static/item2.json'
import item3 from './pages/item-instances/static/item3.json'
import Void from './pages/Void';

import { useEffect } from "react";
import { useLocation } from "react-router-dom";

// Scroll restoration snippet taken from: https://reactrouter.com/web/guides/scroll-restoration
function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}


const Routes = () => (
        <Switch>
            <Route exact path='/'>
                <Home />
            </Route>
            <Route exact path='/home'>
                <Home />
            </Route>
            <Route exact path='/locations'>
                <Location />
            </Route>
            <Route exact path='/stores'>
                <Store />
            </Route>
            <Route exact path='/about'>
                <About />
            </Route>
            <Route exact path='/items'>
                <Item />
            </Route>

            {/* Instance pages (will need to remove). */}

            <Route exact path='/stores/heb'>
                <Heb />
            </Route>
            <Route exact path='/stores/walmart'>
                <StoreList />
            </Route>
            <Route exact path='/stores/target'>
                <Target />
            </Route>
            <Route exact path='/locations/Austin'>
                <Austin />
            </Route>
            <Route exact path='/locations/Dallas'>
                <Dallas />
            </Route>
            <Route exact path='/locations/SanAntonio'>
                <SanAntonio />
            </Route>
            <Route exact path='/stores/walmart/1'>
                <StoreInstance {...walmart1} />
            </Route>
            <Route exact path='/stores/walmart/2'>
                <StoreInstance {...walmart2} />
            </Route>

            <Route exact path='/stores/walmart/3'>
                <StoreInstance {...walmart3} />
            </Route>
            <Route exact path='/items/1'>
                <ItemInstance { ...item1 }/>
            </Route>
            <Route exact path='/items/2'>
                <ItemInstance { ...item2 }/>
            </Route>
            <Route exact path='/items/3'>
                <ItemInstance { ...item3 }/>
            </Route>
            <Route exact path='/void'>
                <Void/>
            </Route>
            <Route>
                <Void/>
            </Route>
        </Switch>  
)
function App() {
    return (
        <>
            <Router>
                <ScrollToTop/>
                <div className='app'>
                    <PageMenu></PageMenu>
                    <Routes></Routes>
                </div>
            </Router>
        </>
    );
}
export default App
