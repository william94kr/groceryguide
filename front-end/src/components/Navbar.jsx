import 'antd/dist/antd.css';
import './styles/navbar.css'
import { Affix, Menu } from 'antd';
import { Link } from 'react-router-dom'
import { UserOutlined, ShoppingOutlined, EnvironmentOutlined, HomeOutlined, CoffeeOutlined } from '@ant-design/icons';

const leftStyle = { 
    position: 'absolute', 
    top: 0, 
    left: 0,
}

const PageMenu = () => (
    <>
        <Affix>
            <Menu mode='horizontal' className='rightStyle'>
                <Menu.Item key='logo' style={ leftStyle } className='logo'>
                    <Link className='link' to='/splash'>
                        <b className='bolded'>Grocery</b>Guide
                    </Link>
                </Menu.Item>
                <Menu.Item key='home' icon={ <HomeOutlined/> } className='customclass'>  
                    <Link className='link' to='/home'>Home</Link>
                </Menu.Item>  
                <Menu.Item key='about' icon={ <UserOutlined className='icon' /> }>
                    <Link className='link' to='/about'>About Us</Link>
                </Menu.Item>
                <Menu.Item key='Item' icon={ <CoffeeOutlined className='icon'/> }>
                    <Link className='link' to='/items'>Items</Link>
                </Menu.Item>
                <Menu.Item key='avail-stores' icon={ <ShoppingOutlined/> }>
                    <Link className='link' to='/stores'>Available Stores</Link>
                </Menu.Item>
                <Menu.Item key='zip Codes' icon={ <EnvironmentOutlined/> }>
                    <Link className='link' to='/locations'>Available Cities</Link>
                </Menu.Item>
            </Menu>
        </Affix>
    </>
);

export default PageMenu