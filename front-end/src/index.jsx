import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

document.body.style = 'background-color: #F5F5F5;';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
